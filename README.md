# Decatur Vote Software
This is software created by Decatur Vote for Decatur Vote's own use. Most of our software is intended for free & open use by the public and other media organizations.

Find us at [DecaturVote.com](https://www.decaturvote.com).

## Using Our Software
Our software packages will include a LICENSE file & you must abide by those licenses. Attribution is always appreciated, but rarely required. To attribute Decatur Vote, use the following:

*This software originally developed by [DecaturVote](https://www.decaturvote.com/). See [their gitlab](https://gitlab.com/decatur-vote/) for more.*

As HTML:
```html
<p><em>This software originally developed by <a href="https://www.decaturvote.com/">DecaturVote</a>. See <a href="https://gitlab.com/decatur-vote/">their gitlab</a> for more.</em></p>
```

## Shared Ownership
All of Decatur Vote's software, unless otherwise stated, is simultaneously owned by Decatur Vote, Reed Sutman, and [Taeluf](https://www.taeluf.com/). While Decatur Vote manages & shares this software, Reed Sutman & Taeluf retain full rights to use, modify, and share the software in any way they see fit. The rights of one entity shall not limit or infringe the rights of another owning entity.
